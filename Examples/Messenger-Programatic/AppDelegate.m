//
//  AppDelegate.m
//  Messenger-Programatic
//
//  Created by Ignacio Romero Zurbuchen on 8/15/14.
//  Copyright (c) 2014 Slack Technologies, Inc. All rights reserved.
//

#import "AppDelegate.h"

#import "SLKTextViewController.h"
#import "MessageViewController.h"
#import "WebViewController.h"


@implementation AppDelegate
            
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    ApiAI *apiai = [ApiAI sharedApiAI];
    
    id <AIConfiguration> configuration = [[AIDefaultConfiguration alloc] init];
    
    configuration.clientAccessToken = @"2b2fae08bed14212b9c699a333189813    ";
    
    apiai.configuration = configuration;
/*
    // Request using text (assumes that speech recognition / ASR is done using a third-party library, e.g. AT&T)
    AITextRequest *request = [self.apiAI textRequest];
    request.query = @[@"hello"];
    [request setCompletionBlockSuccess:^(AIRequest *request, id response) {
        // Handle success ...
    } failure:^(AIRequest *request, NSError *error) {
        // Handle error ...
    }];
    
    [_apiAI enqueue:request];
*/
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    //self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[MessageViewController new]];
    
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[WebViewController new]];

    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
