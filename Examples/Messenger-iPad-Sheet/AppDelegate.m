//
//  AppDelegate.m
//  Messenger-Programatic-iPad-Sheet
//
//  Created by Bob Spryn on 1/23/15.
//  Copyright (c) 2015 Slack Technologies, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import "SLKTextViewController.h"
#import "MessageViewController.h"


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    
    
    self.apiAI = [[ApiAI alloc] init];
    
    // Define API.AI configuration here.
    id <AIConfiguration> configuration = [[AIDefaultConfiguration alloc] init];
    configuration.clientAccessToken = @"YOUR_CLIENT_ACCESS_TOKEN_HERE";
    
    self.apiAI.configuration = configuration;
    
    // Request using text (assumes that speech recognition / ASR is done using a third-party library, e.g. AT&T)
    AITextRequest *request = [self.apiAI textRequest];
    request.query = @[@"hello"];
    [request setCompletionBlockSuccess:^(AIRequest *request, id response) {
        // Handle success ...
    } failure:^(AIRequest *request, NSError *error) {
        // Handle error ...
    }];
    
    [_apiAI enqueue:request];
    
    
    MessageViewController *messageVC = [MessageViewController new];
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:messageVC];
    
    navVC.modalPresentationStyle = UIModalPresentationFormSheet;
    navVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    self.window.rootViewController = [UIViewController new];
    [self.window makeKeyAndVisible];
    
    [self.window.rootViewController presentViewController:navVC animated:YES completion:^{
        [messageVC presentKeyboard:YES];
    }];
    
   
    
    return YES;
}

@end
