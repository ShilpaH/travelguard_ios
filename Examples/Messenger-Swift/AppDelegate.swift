//
//  AppDelegate.swift
//  Messenger
//
//  Created by Ignacio Romero Zurbuchen on 10/16/14.
//  Copyright (c) 2014 Slack Technologies, Inc. All rights reserved.
//

import UIKitself.apiAI = [[ApiAI alloc] init];

// Define API.AI configuration here.
id <AIConfiguration> configuration = [[AIDefaultConfiguration alloc] init];
configuration.clientAccessToken = @"YOUR_CLIENT_ACCESS_TOKEN_HERE";

self.apiAI.configuration = configuration;

// Request using text (assumes that speech recognition / ASR is done using a third-party library, e.g. AT&T)
AITextRequest *request = [self.apiAI textRequest];
request.query = @[@"hello"];
[request setCompletionBlockSuccess:^(AIRequest *request, id response) {
// Handle success ...
} failure:^(AIRequest *request, NSError *error) {
// Handle error ...
}];

[_apiAI enqueue:request];




@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window?.backgroundColor = UIColor.white
        return true
    }
}

