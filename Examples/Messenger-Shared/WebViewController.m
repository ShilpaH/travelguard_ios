//
//  WebViewController.m
//  Messenger
//
//  Created by Shilpa H on 4/17/17.
//  Copyright © 2017 Slack Technologies, Inc. All rights reserved.
//

#import "WebViewController.h"
#import "MessageViewController.h"
#import "MBProgressHUD.h"


@interface WebViewController ()<UIWebViewDelegate>
@property (strong, nonatomic) UIWebView *webView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    /*NSString *urlString = @"https://mvp.travelguard.com/";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    self.webView.delegate = self;
    [_webView loadRequest:urlRequest];*/
    
    UIBarButtonItem *chatWindowButton = [[UIBarButtonItem alloc] initWithTitle:@"Travel Guard"               style:UIBarButtonItemStylePlain target:self action:@selector(openChatWindowButtonAction:)];
    self.navigationItem.rightBarButtonItem = chatWindowButton;
    
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    self.webView.backgroundColor = [UIColor clearColor];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate=self;

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://mvp.travelguard.com/"]];
    request.HTTPMethod = @"POST";
    [self.view addSubview:self.webView];
    [self.webView loadRequest:request];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.label.text = @"Loading..";
   // hud.labelFont = [UIFont fontWithName:@"FuturaPT-Bold" size:16];
   // hud.color = [];//[[AIGTickerUtilities singletonSharedInstance] colorWithHexString:@"008DDF"];
    hud.alpha = 0.9;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) openChatWindowButtonAction:(id)sender
{
    UIStoryboard* mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MessageViewController* messageViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"messageViewControllerID"];
    [self.navigationController pushViewController:messageViewController animated:true];
}

#pragma mark WebView Delegates
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    
    return YES;
    
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:( NSError *)error{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];//:self.view animated:YES];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error",nil) message:NSLocalizedString(@"Page Loading Failed",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    [alertView show];
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
